package info.hccis.teetimebooker.controllers;

import info.hccis.teetimebooker.entity.jpa.Booking;
import info.hccis.teetimebooker.repositories.BookingRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the member functionality of the site
 *
 * @since 20191212
 * @author Fred Campos
 */
@Controller
@RequestMapping("/bookings")
public class BookingController {

    private final BookingRepository bookingRepository;

    public BookingController(BookingRepository br) {
        bookingRepository = br;
    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the bookings from the database.
//        BookingDAO bookingDAO = new BookingDAO();
//        ArrayList<Booking> bookings = bookingDAO.selectAll();
        ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAll();
        model.addAttribute("bookings", bookings);

        return "bookings/list";
    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/add")
    public String add(Model model) {

        Booking booking = new Booking();
        booking.setId(0);
        model.addAttribute("booking", booking);

        return "bookings/add";
    }

    /**
     * Page to allow user to find member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/find")
    public String find(Model model) {

        //Load the names into the model
        Set<String> names = new HashSet();
        
        ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAll();
        for(Booking current: bookings){
            names.add(current.getName1());
        }
                
        model.addAttribute("names", names);
        
        return "bookings/find";
    }

    /**
     * Page to allow user to find a member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

//        BookingDAO bookingDAO = new BookingDAO();
        Optional<Booking> selectedBooking = bookingRepository.findById(id);

        ArrayList<Booking> nidhiyaBookings = bookingRepository.findAllByName1("Nidhiya Nair");
        System.out.println("BJTEST Nidhiya bookings: ");
        for (Booking current : nidhiyaBookings) {
            System.out.println(current.getBookingDate() + " " + current.getBookingTime());
        }

        if (selectedBooking == null) {
            return "index";
        } else {
            model.addAttribute("booking", selectedBooking);
            return "bookings/add";
        }
    }

    /**
     * Page to allow user to delete member bookings
     *
     * @since 20200602
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

//        BookingDAO bookingDAO = new BookingDAO();
//        bookingDAO.delete(id);
        bookingRepository.deleteById(id);

        ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAll();
        model.addAttribute("bookings", bookings);

        //send the user to the list page.
        return "bookings/list";

    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("booking") Booking booking, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "bookings/add";
        }

//        //Apply validation logic.
//        if(booking.getName1().isEmpty()){
//            String message = "name 1 is needed.";
//            model.addAttribute("message", message);
//            model.addAttribute("booking",booking);
//            return "bookings/add";
//        }
//        BookingBO.addBooking(booking);
        bookingRepository.save(booking);

        //reload the list of bookings
//        BookingDAO bookingDAO = new BookingDAO();
        ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAll();
        model.addAttribute("bookings", bookings);

        //send the user to the list page.
        return "bookings/list";
    }

     /**
     * Page to allow user to view a member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/findSubmit")
    public String findSubmit(Model model, @ModelAttribute("booking") Booking booking) {

        System.out.println("BJTEST: about to find "+booking.getName1()+"'s bookings");
        ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAllByName1(booking.getName1());
        model.addAttribute("bookings", bookings);

        //Put the name in the model as well so it can be shown on the list view
        model.addAttribute("findNameMessage", " ("+booking.getName1()+")");

        
        //send the user to the list page.
        return "bookings/list";
    }

    
    
}
