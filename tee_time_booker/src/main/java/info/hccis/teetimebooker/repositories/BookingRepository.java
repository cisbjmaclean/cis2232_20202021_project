package info.hccis.teetimebooker.repositories;

import info.hccis.teetimebooker.entity.jpa.Booking;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Integer> {
        ArrayList<Booking> findAllByName1(String name);
}