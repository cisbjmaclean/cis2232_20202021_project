DROP DATABASE if exists cis2232_teetime;
CREATE DATABASE cis2232_teetime;
USE cis2232_teetime;

CREATE TABLE Booking (
  id int(5) NOT NULL,
  name1 varchar(100) NOT NULL COMMENT 'Booker name',
  name2 varchar(100) DEFAULT NULL COMMENT 'Player 2',
  name3 varchar(100) DEFAULT NULL COMMENT 'Player 3',
  name4 varchar(100) DEFAULT NULL COMMENT 'Player 4',
  bookingDate varchar(10) DEFAULT NULL COMMENT 'yyyy-MM-dd',
  bookingTime varchar(5) DEFAULT NULL COMMENT 'hh:mm',
  courseName varchar(100) DEFAULT NULL COMMENT 'Course name',
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When record was created. yyyy-MM-dd hh:mm'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table booking
--

INSERT INTO Booking (id, name1, name2, name3, name4, bookingDate, bookingTime, courseName, createdDateTime) VALUES
(1, 'Nidhiya Nair', 'Jeff Thistle', 'Bo Lan', 'Dylan Corriveau', '2020-05-18', '08:00', 'Summerside', '2020-05-14 15:07'),
(2, 'Kendall Fowler', 'Omar Bakraky', 'Joey Abou Risk', 'Thao Ho', '2020-05-18', '08:10', 'Summerside', '2020-05-14 15:07'),
(3, 'Nicole Ross', 'BJ MacLean', 'Mariana Alkabalan', '', '2020-05-18', '08:20', 'Summerside', '2020-05-14 15:07');

ALTER TABLE Booking
  ADD PRIMARY KEY (id);

ALTER TABLE Booking
  MODIFY id int(5) NOT NULL AUTO_INCREMENT;
COMMIT;
