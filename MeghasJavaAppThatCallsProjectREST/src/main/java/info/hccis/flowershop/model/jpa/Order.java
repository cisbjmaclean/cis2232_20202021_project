package info.hccis.flowershop.model.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Parking pass class
 *
 * @author mmegha
 * @since 20201113
 */
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer customerId;
    private String orderDate;
    private Integer item1;
    private Integer item2;
    private Integer item3;
    private Integer item4;
    private Integer item5;
    private Integer item6;
    private Integer orderStatus;
    private BigDecimal totalCost;
    private BigDecimal amountPaid;

    
    public Order() {
    }

    public Order(Integer id) {
        this.id = id;
    }

    public Order(Integer id, Integer customerId, String orderDate, Integer item1, Integer item2, Integer item3, Integer item4, Integer item5, Integer item6, Integer orderStatus, BigDecimal totalCost, BigDecimal amountPaid) {
        this.id = id;
        this.customerId = customerId;
        this.orderDate = orderDate;
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
        this.orderStatus = orderStatus;
        this.totalCost = totalCost;
        this.amountPaid = amountPaid;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getItem1() {
        return item1;
    }

    public void setItem1(Integer item1) {
        this.item1 = item1;
    }

    public Integer getItem2() {
        return item2;
    }

    public void setItem2(Integer item2) {
        this.item2 = item2;
    }

    public Integer getItem3() {
        return item3;
    }

    public void setItem3(Integer item3) {
        this.item3 = item3;
    }

    public Integer getItem4() {
        return item4;
    }

    public void setItem4(Integer item4) {
        this.item4 = item4;
    }

    public Integer getItem5() {
        return item5;
    }

    public void setItem5(Integer item5) {
        this.item5 = item5;
    }

    public Integer getItem6() {
        return item6;
    }

    public void setItem6(Integer item6) {
        this.item6 = item6;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Order)) {
            return false;
        }
        Order other = (Order) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Id: " + id  + " Customer Id: "+ customerId
                + " Order date: "+ orderDate +  " Welcome Baby Boy: "+ item1 +
                " Welcome Baby Girl: " + item2 + " Very Special Delivery: " + item3+
                " Small Stuffed Animal: "+ item4 + " Medium Stuffed Animal: "+ item5+
                " Large Stuffed Animal: "+item6+ " Order Status:"+ orderStatus+
                " Total Cost: "+ totalCost +" Amount Paid: "+ amountPaid;
                
    }
    
}
