package info.hccis.flowershop.model.jpa;

import java.io.Serializable;

/**
 * Customer class
 * 
 * @author MSII
 * @since 20201117
 */
public class Customer implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Integer id;
    private int customerTypeId;
    private String name;
    private String address;
    private String city;
    private String province;
    private String postalCode;
    private String phoneNumber;
    private String birthDate;
    private String loyaltyCard;
    
    private String customerTypeDescription;

    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }
    
    public Customer(){
        //Default constructor
    }

    public Customer(Integer id, int customerTypeId, String name, String address, 
            String city, String province, String postalCode, String phoneNumber, 
            String birthDate, String loyaltyCard) {
        this.id = id;
        this.customerTypeId = customerTypeId;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.loyaltyCard = loyaltyCard;
    }
    
    //Getters and setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(int customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLoyaltyCard() {
        return loyaltyCard;
    }

    public void setLoyaltyCard(String loyaltyCard) {
        this.loyaltyCard = loyaltyCard;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer customer2 = (Customer) object;
        if ((this.id == null && customer2.id != null) || (this.id != null && !this.id.equals(customer2.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Customer Information : \n" + "Customer ID : " + id + "\nType ID : " + customerTypeId 
                + "\nName : " + name + "\nAddress : " + address + "\nCity : " + city 
                + "\nProvince : " + province + "\nPostal Code : " + postalCode + "\nPhone Number : " 
                + phoneNumber + "\nBirthday : " + birthDate + "\nLoyalty Card # : " + loyaltyCard;
    }
}
